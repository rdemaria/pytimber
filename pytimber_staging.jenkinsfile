pipeline {
  agent {label 'slave2'}

  stages {
    stage ('Check development build') {

      steps {
        script {
             def rx = httpRequest authentication: 'e8863ac5-1b33-4dc1-bbbd-8068aecd3263', url: "$JENKINS_MASTER_URL/job/pytimber-develop/lastBuild/api/json"
             def props = readJSON text: rx.getContent()
             println props['result']
        }
      }
    }

    stage ('Checkout') {
      steps {
        //fresh checkout every time
        deleteDir()
        git url: 'ssh://git@gitlab.cern.ch:7999/acc-logging-team/pytimber.git', branch: params.GIT_SOURCE_BRANCH_NAME
        script{
            env.GIT_SOURCE_BRANCH_NAME = params.GIT_SOURCE_BRANCH_NAME
        }
      }
    }

    stage('Integration test') {
        steps {
            script {
                env.JENKINS_ENV = 'CI'
                withCredentials([file(credentialsId: 'NXCALS-ANSIBLE-VAULT-PASS', variable: 'VAULT_PASS')]) {
                    println "Integration tests..."
                    sh './integration-tests.sh'
                }
            }
        }
    }

    stage('Promote to PRO?') {
        agent { label 'master'}
        steps {
            timeout(time: 12, unit: 'HOURS') {
                input message: 'Do you want to promote this build to PRO?'
            }
        }
    }

    stage('Change & commit the version') {
        steps {
            script {
                sh '''
                    source $WORKSPACE/venv/bin/activate
                    if [ -z "${FORCE_VERSION}" ]
                    then
                        python -m pip install setuptools_scm
                        VERSION=`python ./next_version.py`
                    else
                        VERSION=${FORCE_VERSION}
                    fi
                    git checkout master
                    git pull --rebase
                    git rebase $GIT_SOURCE_BRANCH_NAME
                    git push
                    git tag -a v$VERSION -m "Version: v$VERSION"
                    git push origin tag v$VERSION
                '''
            }
        }
    }

    stage('Release sdist') {
		steps {
		    println "Releasing sdist..."
		    println pwd()
		    println env.WORKSPACE
 		    sh '''
 		        source $WORKSPACE/venv/bin/activate
                python -m pip install twine build
                pip install --upgrade pip
		        python -m pip install -U setuptools
		        python -m build --sdist .
		        python -m twine upload --repository-url $PYPI_UPLOAD_URL -u $PYPI_USERNAME -p $PYPI_PASSWORD dist/*.tar.gz
		    '''
		}
    }

    stage('Build wheel') {
        steps {
            sh '''
                source $WORKSPACE/venv/bin/activate
                # cd ${project_root}
                mkdir -p dist && cd dist && pip wheel ../ --no-deps && cd ../
                mkdir wheelhouse
                if [ "$(ls -A ./dist/*-any.whl)" ]; then
                    # Pure Python wheel. No need to audit it.
                    mv ./dist/*-any.whl ./wheelhouse/
                else
                    apt-get update && apt-get install patchelf;
                    python -m pip install auditwheel;
                    auditwheel repair dist/*.whl;
                fi
            '''
        }
    }

    stage('Release wheel') {
		steps {
		    println "Releasing wheel..."
		    sh '''
		        source $WORKSPACE/venv/bin/activate
		        python -m twine upload --repository-url ${PYPI_UPLOAD_URL} -u ${PYPI_USERNAME} -p ${PYPI_PASSWORD} wheelhouse/*.whl
		        # maybe instead of above we should use:
		        # acc-py devrelease "$FILENAME"  # where filename is a wheel
		    '''
		}
    }
  }// stages

   post {
        // always {
        //     echo 'I will always say Hello again!'
        // }
        failure {
                 emailext attachLog: true,
                         recipientProviders: [[$class: 'CulpritsRecipientProvider'], [$class: 'DevelopersRecipientProvider']],
                         to: 'acc-logging-team@cern.ch',
                         subject: 'PYTIMBER staging build failed',
                         body: 'Staging build ${BUILD_URL} failed for PYTIMBER.'
        }
    }
}
