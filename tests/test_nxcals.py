import pytimber


def test_nxcals_init_local():
    nxcals = pytimber.NXCals()
    assert nxcals.spark.version()
    nxcals = pytimber.NXCals(master="yarn")
    assert nxcals.spark.version()


def test_nxcals_init_small():
    nxcals = pytimber.NXCals(sparkconf="small")
    assert nxcals.spark.version()
    nxcals = pytimber.NXCals(sparkconf="small", master="yarn")
    assert nxcals.spark.version()


def test_nxcals_init_dict():
    conf = {"spark.executor.memory": "2G"}
    nxcals = pytimber.NXCals(sparkconf=conf)
    assert nxcals.spark.version()
    nxcals = pytimber.NXCals(sparkconf=conf, master="yarn")
    assert nxcals.spark.version()


def test_nxcals_init_flavour_and_dict():
    conf = {"spark.executor.memory": "2G"}
    nxcals = pytimber.NXCals(sparkconf=conf)
    assert nxcals.spark.version()
    nxcals = pytimber.NXCals(sparkconf="small", master="yarn", sparkprops=conf)
    assert nxcals.spark.version()


def test_nxcals_search_variable():
    nxcals = pytimber.NXCals()
    lst = nxcals.searchVariable("LHC%BCT%INT%")
    assert len(lst) > 30


def test_nxcals_get_variable():
    nxcals = pytimber.NXCals()
    t1 = "2018-05-23 00:05:54.500"
    t2 = "2018-05-23 00:06:54.500"
    ts, val = nxcals.getVariable("LHC.BCTFR.A6R4.B1:BEAM_INTENSITY", t1, t2)
    assert len(ts) > 30
    assert len(ts) == len(val)
    assert val.dtype.str == "<f8"


def test_nxcals_spark_extraction():
    nxcals = pytimber.NXCals()
    t1 = "2018-05-23 00:05:54.500"
    t2 = "2018-05-23 00:06:54.500"
    ds = nxcals.getVariable("LHC.BCTFR.A6R4.B1:BEAM_INTENSITY", t1, t2, output="spark")
    rows = (
        ds.select("nxcals_timestamp", "nxcals_value")
        .na()
        .drop()
        .sort("nxcals_timestamp")
        .collect()
    )
    ts, val1 = list(zip(*[(row.get(0) / 1000.0, row.get(1)) for row in rows]))
    arr = nxcals.rows2array(rows)
    pd = nxcals.rows2pandas(rows)
    assert len(arr) == len(pd)


def text_nxcals_spark2numpy():
    nxcals = pytimber.NXCals()
    df = (
        nxcals.DataQuery.byEntities()
        .system("CMW")
        .startTime("2018-04-29 00:00:00.000")
        .endTime("2018-04-30 00:00:00.000")
        .entity()
        .keyValue("device", "LHC.LUMISERVER")
        .keyValue("property", "CrossingAngleIP1")
        .buildDataset()
    )

    arr = nxcals.spark2numpy(df)
    pd = nxcals.spark2pandas(df)
    assert len(arr) == len(pd)
